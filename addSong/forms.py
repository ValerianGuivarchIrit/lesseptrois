from django import forms


class URLSongOffline(forms.Form):
    name = forms.CharField(label='Votre nom', max_length=50)
    url = forms.URLField(label='URL vidéo', max_length=250)


class URLSongOnline(forms.Form):
    url = forms.URLField(label='URL vidéo', max_length=250)


class Connexion(forms.Form):
    mail = forms.EmailField(label='Email', max_length=250)
    mdp = forms.CharField(label='Mot de passe', max_length=50, widget=forms.PasswordInput)
