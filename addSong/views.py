import time

from django.shortcuts import render
from django.template import loader
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

from LesSeptRois.settings import YOUTUBE_DATA_API_KEY
from addSong import forms
from datetime import datetime
import pyrebase
import requests.exceptions
import json

import os

import googleapiclient.discovery



# Turn on HTTPS/SSL access.
# Note: SSL is not available at this time for uploads.
#yt_service = gdata.youtube.service.YouTubeService()
#yt_service.ssl = True
#yt_service.developer_key = YOUTUBE_DATA_API_KEY
#yt_service.client_id = 'Wake Me Up !'


def GetAndPrintVideoFeed(id):

    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"
    api_service_name = "youtube"
    api_version = "v3"
    DEVELOPER_KEY = YOUTUBE_DATA_API_KEY

    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, developerKey=DEVELOPER_KEY)

    request = youtube.videos().list(
        part="snippet",
        id=id
    )
    response = request.execute()

    return response


# Configuration de la base de données
config = {
    'apiKey': 'AIzaSyAOuQX3BaGHDssOX7jeLCq56IdailOZE-c',
    'authDomain': 'wake-me-up-35646.firebaseio.com/',
    'databaseURL': 'https://wake-me-up-35646.firebaseio.com/',
    'storageBucket': 'wakemeup-d8f76.appspot.com',
}

firebase = pyrebase.initialize_app(config)


"""
***********************************************************************************************************************
"""


# TODO la connexion doit etre en POST pour pas montrer l ID
# Permet de récupérer les information dans la classe DemandeMusique de la BDD
def processRequest(idDemande):
    db = firebase.database()
    userID = ''
    username = ''
    all_requests = db.child("LienMusicMe").get()
    for request in all_requests.each():
        if request.key() == idDemande:
            userID = request.val()['userID']
            username = request.val()['username']
    return userID, username


"""
***********************************************************************************************************************
"""


# Connecte un utilisateur au site
def connect(request, idDemande):

    template = loader.get_template('addSong/connexion.html')
    db = firebase.database()
    auth = firebase.auth()
    senderId = ''

    if request.method == 'POST':
        form = forms.Connexion(request.POST)

        if form.is_valid():
            mail = form.cleaned_data['mail']
            mdp = form.cleaned_data['mdp']

            try:
                # Authentification
                user = auth.sign_in_with_email_and_password(mail, mdp)

                # Récupération du pseudo de l'utilisateur
                # J'aimerais le faire depuis la variable 'user' mais j'y arrive pas
                all_users = db.child("Users").get()
                for info_user in all_users:
                    if info_user.val()['mail'] == mail:
                        senderId = info_user.val()['id']

                return HttpResponseRedirect(reverse('main', args=[idDemande, 'online', senderId]))

            except requests.exceptions.HTTPError as httpErr:
                error_message = json.loads(httpErr.args[1])['error']['message']
                print(error_message)

    else:
        form = forms.Connexion()

    context = {
        'form': form,
    }

    return HttpResponse(template.render(context, request))


"""
***********************************************************************************************************************
"""


# Ajoute une relation dans la base de données Sonnerie / Song
def addSongDB(idUser, url, sender, senderId):

    db = firebase.database()
    id = url.split('watch?v=')[1]
    #TODO surement pas suffisant

    entry = GetAndPrintVideoFeed(id)
    print(entry)
    print(entry.get("items")[0].get("snippet").get("title"))

    Song = {
        'id': id,
        'title': entry.get("items")[0].get("snippet").get("title"),
        'artworkUrl': entry.get("items")[0].get("snippet").get("thumbnails").get("default").get("url")
    }

    Sonnerie = {
        'idSong': id,
        'dateEnvoie': int(round(time.time() * 1000)),
        'ecoutee': False,
        'idReceiver': idUser,
        'senderId': senderId,
        'senderName': sender,
    }

    db.child("Song").child(id).set(Song)
    db.child("Sonnerie").push(Sonnerie)


"""
***********************************************************************************************************************
"""


# Génère la page principale pour ajouter une musique au réveil d'un utilisateur
def add(request, idDemande, connection="offline", senderId=''):

    template = loader.get_template('addSong/index.html')
    idUser, name = processRequest(idDemande)
    sender = ''
    form = ''

    if request.method == 'POST':

        if connection == 'offline':
            form = forms.URLSongOffline(request.POST)
        elif connection == 'online':
            form = forms.URLSongOnline(request.POST)

        if form.is_valid():
            url = form.cleaned_data['url']

            if connection == 'offline':
                sender = form.cleaned_data['name']

            addSongDB(idUser, url, sender, senderId)

            return HttpResponseRedirect(reverse('thanks', args=[name]))

    else:
        if connection == 'offline':
            form = forms.URLSongOffline()
        elif connection == 'online':
            form = forms.URLSongOnline()

    context = {
        'idDemande': idDemande,
        'name': name,
        'idUser': idUser,
        'sender': sender,
        'connection': connection,
        'form': form,
    }

    return HttpResponse(template.render(context, request))


"""
***********************************************************************************************************************
"""


# Génère la page qui confirme l'ajout de la musique
def thanks(request, name):

    template = loader.get_template('addSong/thanks.html')
    context = {
        'name': name,
    }

    return HttpResponse(template.render(context, request))
